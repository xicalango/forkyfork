
fun sum(v1: Int, v2: Int) = v1 + v2

fun <T, R> applyAll(mapper: (List<T>) -> R, nestedList: List<List<T>>): List<R> = nestedList.map(mapper)

fun foldInt(op: (Int, Int) -> Int, list: List<Int>) = list.fold(0, op)

fun sorted(list: List<Int>) = list.sorted()
fun sortedRev(list: List<Int>) = list.sortedDescending()

fun take(num: Int, list: List<Int>) = list.take(num)

fun main() {

    val data = listOf(
            listOf(1000, 2000, 3000),
            listOf(4000),
            listOf(5000, 6000),
            listOf(7000, 8000, 9000),
            listOf(10000)
    )

    val sumUp = ::foldInt `#` ::sum
    val sums = partial(::applyAll, sumUp)
    val solutionA = (::foldInt `#` ::maxOf) `$` sums

    val solutionB = sumUp `$` (::take `#` 3 ) `$` ::sortedRev `$` sums

    println(solutionA(data))
    println(solutionB(data))
}
