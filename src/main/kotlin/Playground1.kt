import kotlin.math.max

// https://www.youtube.com/watch?v=U6I-Kwj-AvY

fun main() {
    val lists = listOf(
            listOf(-2, -1, -1, 1, 2, 3),
            listOf(-3, -2, -1, 0, 0, 1, 2),
            listOf(5, 20, 66, 1314)
    )

    val op = fork<List<Int>, Int, Int, Int>(
            { it.map(Int::isNegative).sumBy(Boolean::toInt) },
            ::max,
            { it.map(Int::isPositive).sumBy(Boolean::toInt) }
    )

    val opPsiReduction = fork<List<Int>, List<Boolean>, List<Boolean>, Int>(
            { it.map(Int::isNegative) },
            psi(::max) { it.sumBy(Boolean::toInt) },
            { it.map(Int::isPositive) }
    )

    val opPsiReductionPartial = fork(
            partial(::map, Int::isNegative),
            psi(::max, partial(::sumBy, Boolean::toInt)),
            partial(::map, Int::isPositive)
    )

    val opPsiReductionPartialComp1 = { cmp: Int ->
        fork<List<Int>, List<Boolean>, List<Boolean>, Int>(
                partial(::map) { compareLt(it, cmp) },
                psi(::max, partial(::sumBy, Boolean::toInt)),
                partial(::map) { compareGt(it, cmp) }
        )
    }(0)

    val opPsiReductionPartialComp2 = swapArgs(forkS<List<Int>, List<(Int) -> Boolean>, (Int) -> Int>(
            { list -> list.map { element -> { compareLt(element, it) } } },
            { list1, list2 ->
                { cmp -> max(list1.map { it(cmp) }.sumBy { it.toInt() }, list2.map { it(cmp) }.sumBy { it.toInt() }) }
            },
            { list -> list.map { element -> { compareGt(element, it) } } },
    ))(0)

    val opPsiReductionPartialComp = swapArgs(fork(
            partial(::map, curry(::compareGt)),
            psi(
                    curryFork(::max),
                    compose(composeCurry(partial(::sumBy, Boolean::toInt)), ::listApply)
            ),
            partial(::map, curry(::compareLt)),
    ))(0)

    lists.forEach { println(op(it)) }
    lists.forEach { println(opPsiReduction(it)) }
    lists.forEach { println(opPsiReductionPartial(it)) }
    lists.forEach { println(opPsiReductionPartialComp1(it)) }
    lists.forEach { println(opPsiReductionPartialComp2(it)) }
    lists.forEach { println(opPsiReductionPartialComp(it)) }

}