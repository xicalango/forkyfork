import kotlin.math.max

fun <T, I1, I2, R> fork(op1: (T) -> I1, combine: (I1, I2) -> R, op2: (T) -> I2): (T) -> R =
        { combine(op1(it), op2(it)) }

fun <T1, T2, I1, I2, R> bifork(op1: (T1, T2) -> I1, combine: (I1, I2) -> R, op2: (T1, T2) -> I2): (T1, T2) -> R = { v1, v2 ->
    combine(op1(v1, v2), op2(v1, v2))
}

fun <T, I, R> forkS(op1: (T) -> I, combine: (I, I) -> R, op2: (T) -> I): (T) -> R = fork(op1, combine, op2)

fun <T, I, R> curryFork(combine: (I, I) -> R): ((T) -> I, (T) -> I) -> (T) -> R = { op1, op2 -> fork(op1, combine, op2) }

fun <T1, T2, R> psi(combine: (T1, T1) -> R, reduction: (T2) -> T1): (T2, T2) -> R =
        { v1, v2 -> combine(reduction(v1), reduction(v2)) }

fun <T1, R> curry(f: (T1) -> R): () -> (T1) -> R = { { a1 -> f(a1) } }

fun <T1, T2, R> curry(f2: (T1, T2) -> R): (T1) -> (T2) -> R =
        { a1 -> { a2 -> f2(a1, a2) } }

fun <T1, T2, T3, R> curry3(f: (T1, T2, T3) -> R): (T1) -> (T2) -> (T3) -> R =
        { a1 -> { a2 -> { a3 -> f(a1, a2, a3) } } }

fun <T1, T2, R> swapArgs(f2: (T1, T2) -> R): (T2, T1) -> R = { a2, a1 -> f2(a1, a2) }

fun <T1, T2, R> partial(f2: (T1, T2) -> R, arg: T1): (T2) -> R = curry(f2)(arg)

infix fun <T1, T2, R> ((T1, T2) -> R).`#`(arg: T1): (T2) -> R = partial(this, arg)

fun <T1, T2, R> swapArgs(f: (T1) -> (T2) -> R): (T2) -> (T1) -> R = { a2 -> { a1 -> f(a1)(a2) } }

fun <T, R> listApply(list: List<(T) -> R>): (T) -> List<R> = { value -> list.map { it(value) } }

fun <T, I, R> compose(f: (I) -> R, g: (T) -> I): (T) -> R = { input -> f(g(input)) }

infix fun <T, I, R> ((I) -> R).`$`(g: (T) -> I): (T) -> R = compose(this, g)

fun <T, I, R> composeCurry(f: (I) -> R): ((T) -> I) -> (T) -> R = { g -> { input -> f(g(input)) } }


fun Boolean.toInt() = if (this) 1 else 0

fun compareLt(arg1: Int, arg2: Int) = arg1 < arg2
fun compareGt(arg1: Int, arg2: Int) = arg1 > arg2

fun Int.isNegative() = compareLt(this, 0)
fun Int.isPositive() = compareGt(this, 0)

fun <T, R> map(mapper: (T) -> R, list: List<T>): List<R> = list.map(mapper)
fun <T> sumBy(selector: (T) -> Int, list: List<T>): Int = list.sumBy(selector)
