fun idiv(i1: Int, i2: Int): Int = i1 / i2

fun strlen(str: String): Int = str.length

fun strtake(len: Int, str: String): String = str.take(len)
fun strdrop(len: Int, str: String): String = str.drop(len)

fun strinter(str1: String, str2: String): String = String(str1.toCharArray().toSet().intersect(str2.toCharArray().toSet()).toCharArray())

fun <T> identity(): (T) -> T = { it }

fun strdiadicIota(s1: String, s2: String): Int = s1.indexOf(s2) + 1

const val LETTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

fun main() {

    val data = """
        vJrwpWtwJgWrhcsFMMfFFhFp
        jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
        PmmdzqPrVvPwwTWBwg
        wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
        ttgJtRGJQctTZtZT
        CrZsJsPPZsGzwwsLwLmpwMDw
    """.trimIndent().lines()

    val splitOp = swapArgs(::idiv) `#` 2 `$` ::strlen

    val data1 = data[0]

    val n = splitOp(data1)

    println((::strtake `#` n)(data1))
    println((::strdrop `#` n)(data1))

    println(strinter((::strtake `#` n)(data1), (::strdrop `#` n)(data1)))

    val op = fork(
            ::strtake `#` n,
            ::strinter,
            ::strdrop `#` n,
    )

    val oop = bifork(
            ::strtake,
            ::strinter,
            ::strdrop
    )

    val ooop = fork(
            swapArgs(::idiv) `#` 2 `$` ::strlen,
            bifork(
                    ::strtake,
                    ::strinter,
                    ::strdrop
            ),
            identity()
    )

    println(ooop(data1))

    println(partial(::map, ooop)(data))

    val solutionA = ::foldInt `#` ::sum `$`
            partial(::map, ::strdiadicIota `#` LETTERS) `$`
            partial(::map, fork(
                    swapArgs(::idiv) `#` 2 `$` ::strlen,
                    bifork(::strtake, ::strinter, ::strdrop),
                    identity()
            ))

    println(solutionA(data))

}
